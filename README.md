# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://adamitsch@bitbucket.org/adamitsch/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/adamitsch/stroboskop/commits/ca6b7bdd31ac4bbb8b653d13a4de1f8131b0bb3e

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/adamitsch/stroboskop/commits/569046d5337a52031ed042232b23160b6d92f786

Naloga 6.3.2:
https://bitbucket.org/adamitsch/stroboskop/commits/48453f5a8ddec837a8e2e26d4f6f8c76d62d687a

Naloga 6.3.3:
https://bitbucket.org/adamitsch/stroboskop/commits/29778ca3d45c198c66bb5536d27a26038c975533

Naloga 6.3.4:
https://bitbucket.org/adamitsch/stroboskop/commits/f208aea8897cde108b5a0ec4e8de870e578ffaf8

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/adamitsch/stroboskop/commits/7cb7605fa38fd9c2edc5c925223e3aaa7f76d1d1

Naloga 6.4.2:
https://bitbucket.org/adamitsch/stroboskop/commits/b02fe244e9afd7a62cabd5cc81356f6ec513b491

Naloga 6.4.3:
https://bitbucket.org/adamitsch/stroboskop/commits/19de885dc522ee0691e610439c354b5224f227fe

Naloga 6.4.4:
https://bitbucket.org/adamitsch/stroboskop/commits/809ab71dd18785043efe5b812d51e19b7ee2fdb8
